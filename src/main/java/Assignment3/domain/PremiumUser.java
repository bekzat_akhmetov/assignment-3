package Assignment3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;

public class PremiumUser extends User {
    private int id;
    private boolean premium;
    private int balance=0;
    File file = new File("Beka2.txt");
    Scanner fileScanner = new Scanner(file);
    public PremiumUser() throws FileNotFoundException {
    }

    public PremiumUser(int id, int balance) throws FileNotFoundException {
        setId(id);
        setBalance(balance);
    }


    public PremiumUser(int id, String name, String surname, String username, String password, boolean premium) throws FileNotFoundException {
        super(id, name, surname, username, password);
        setPremium(premium);
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public boolean isPremium() {
        return premium;
    }


    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }
    
     @Override
    public String toString() {
        return name+" "+ surname+" "+username+" "+" "+password+" "+premium+" "+super.toString();
    }
}
