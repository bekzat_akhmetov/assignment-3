package Assignment3.domain;

import org.w3c.dom.ls.LSOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    private ArrayList<PremiumUser> users;
    private Friends friend;
    private Scanner sc = new Scanner(System.in);
    private int balance=0;
    private PremiumUser signedUser;
    int previousTransaction;
    private String Username;
    public MyApplication() {
        users = new ArrayList<PremiumUser>();
    }

    private void fillUsers() throws FileNotFoundException {
        File file = new File(("Beka.txt"));
        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            users.add(new PremiumUser(fsc.nextInt(), fsc.next(), fsc.next(), fsc.next(), fsc.next(),fsc.hasNextBoolean()));
        }
    }
    private void fillUsers1() throws FileNotFoundException {
        File file = new File("Beka2.txt");
        Scanner fsc = new Scanner(file);
        while (fsc.hasNext()) {
            users.add(new PremiumUser(fsc.nextInt(), fsc.nextInt()));
        }
    }

    public void start() throws IOException {
        fillUsers();
        while (true) {
            System.out.println("Welcome to the application");
            System.out.println("Select command");
            System.out.println("1.Menu");
            System.out.println("2.Exit");
            int choice = sc.nextInt();
            if (choice == 1) menu();
            else break;
        }

    }
    protected void userProfile() throws IOException {
        while (true) {
            PremiumUser buyPremium=new PremiumUser();
            System.out.println("Hello" + " "+Username);
            System.out.println("1)Change password");
            System.out.println("2)Get the premium");
            System.out.println("3)Log off");
            int choice = sc.nextInt();
            if (choice == 1) {
                changePassword();
            } else if (choice == 2) {
                buiyngpremium();
            } else if(choice==3){
                logoff();
            }
            else break;
        }
    }

    protected void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in");
                System.out.println("1.Authentication");
                System.out.println("2.Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void addUser(PremiumUser user) throws FileNotFoundException {
        users.add(user);
        fillUsers();
    }


    protected void changePassword() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your new password");
        String password = sc.next();
        System.out.println("Repeat your new password");
        String password1 = sc.next();
        if (password.equals(password1)) {
            System.out.println("Your password successfully changed");
        }
        else System.out.println("your passwords are not same! Try again!");
    }

    private void logoff() throws IOException {
        signedUser = null;
        authentication();
    }
    private boolean buyingPremium() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Amount of premium is 500$");
        System.out.println("1)Buy");
        File file = new File("Beka2.txt");
        Scanner fsc = new Scanner(file);
        System.out.println("2)Go back to menu");
        int amount = 500;
        int choice = sc.nextInt();
        if (choice == 1) {
            PremiumUser user = new PremiumUser();
            System.out.println("Write your username");
            String username = sc.next();
            System.out.println("Write your password");
            String password = sc.next();
            for (int i = 0; i < users.size(); i++) {
                if ( users.get(i).getUsername().equals(username) && users.get(i).getPassword().equals(password)) {
                    while (fsc.hasNextInt()) {
                        int id = fsc.nextInt();
                        balance = fsc.nextInt();
                        if (balance >= 500) {
                            balance = balance - amount;
                            previousTransaction = -amount;
                            System.out.println("Congratulations you've got premium");
                            System.out.println("Your balance: " + balance);
                            users.add(user);
                            return true;
                        } else System.out.println("Not enough money");
                        buyingPremium();
                        return false;
                    }
                }
            }
        }
        else if (choice == 2) {
            start();
        }
        return false;
    }

    public void buiyngpremium() throws IOException {
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.println("1)buying premium");
            System.out.println("2)Deposit");
            System.out.println("3)show the previous transaction");
            System.out.println("4)Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                buyingPremium();
            } else if (choice == 2) {
                deposit();
            } else if (choice == 3) {
                getPreviousTransaction();
            } else break;

        }
    }
    protected void authentication() throws IOException {
        System.out.println("1)Sign up");
        System.out.println("2)Sign in");
        System.out.println("3)back to menu");
        int choice = sc.nextInt();
        if (choice == 1) signup();
        else if (choice == 2) signin();
        else menu();
    }

    protected void signin() throws IOException {
        PremiumUser user = new PremiumUser();
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your username");
        String username = sc.next();
        System.out.println("Write your password");
        String password = sc.next();
        for(int i=0;i<users.size();i++) {
            if (users.get(i).getUsername().equals(username) && users.get(i).getPassword().equals(password)) {
                System.out.println("You successfully signed in");
                signedUser = user;
                Username=username;
                userProfile();
            }
        }
             System.out.println("Your log in or password is incorrect, try again");
            signin();
    }

    private void getPreviousTransaction() {
        if (previousTransaction > 0) {
            System.out.println("Deposit: " + previousTransaction);
        } else if (previousTransaction < 0) {
            System.out.println("Withdraw: " + Math.abs(previousTransaction));
        } else System.out.println("No transaction occured");
    }
    private boolean checkingPremium() throws IOException {
        return buyingPremium();
    }

    private void deposit() throws IOException {
        User s = new User();
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your id,username,password");
        int id = sc.nextInt();
        String username = sc.next();
        String password = sc.next();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == id && users.get(i).getUsername().equals(username) && users.get(i).getPassword().equals(password)) {
                System.out.println("Write your sum to deposit to your account");
                int amount = sc.nextInt();
                if (amount > 0) {
                    balance=+ amount;
                    previousTransaction = amount;
                    fillUsers1();
                    System.out.println();
                } else System.out.println("Error");
            }
        }
        System.out.println("your username or password is incorrect. Please try again");
        MyApplication m = new MyApplication();
        m.start();
    }

    protected void signup() throws IOException {
        PremiumUser user = new PremiumUser();
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your name");
        String name = sc.next();
        System.out.println("Write you surname");
        String surname = sc.next();
        System.out.println("Write your username");
        String username = sc.next();
        System.out.println("Write you password");
        String password = sc.next();
        System.out.println("Repeat your password");
        String password1 = sc.next();
        if ( password.equals(password1)) {
            if(!user.checkPassword(password)) {
                System.out.println("your password has to contain more than 7 words, at least one upper case,at least one lower case");
            }
            else if(user.checkPassword(password)){
                user.setPassword(password);
                user.setName(name);
                user.setSurname(surname);
                user.setUsername(username);
                addUser(user);
                signedUser = user;
                System.out.println("You registered succesfully!");
                saveNewUser();
            }
        }
        else System.out.println("your passwords are not same, please repeat");
    }


    private void saveNewUser() throws IOException {
        String content = "";
        for (PremiumUser user: users) {
            content=content +  "\n" + user.getId() + " " + user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword() + user.isPremium()+"\n";
        }
        Files.write(Paths.get("C:\\Bekzat.txt"), content.getBytes());
    }
}
