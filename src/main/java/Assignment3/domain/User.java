package Assignment3.domain;

public class User {
    protected int id;
    protected static int idGen=0;
    protected String name;
    protected String surname;
    protected String username;
    protected String password;
    public User(){
        this.id=idGen++;
    }

    public User(int id,int id1){
        setId(id);
        setId(id1);
    }

    public User(int id,String name,String surname,String username,String password){
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        if(idGen<id+1){
            idGen=id+1;
        }
    }
    public User(String name,String surname,String username,String password){
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (checkPassword(password)) this.password = password;
    }

    @Override
    public int hashCode() {
        return password!=null?password.hashCode():0;
    }

    public boolean checkPassword(String password) {
        boolean UpperCase = false;
        boolean LowerCase = false;
        boolean hasDigit = false;
        if (password.length() > 7) {
            for (int i = 0; i < password.length(); i++) {
                char curChar = password.charAt(i);
                if (Character.isUpperCase(curChar)) UpperCase = true;
                else if (Character.isLowerCase(curChar)) LowerCase = true;
                else if (Character.isDigit(curChar)) hasDigit = true;
            }
            return UpperCase && LowerCase && hasDigit;
        }
        return false;
    }

}
